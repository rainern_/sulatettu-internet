# Huomiot
- Kyseinen projekti on kouluprojekti. Eli dokumentaatio on kirjoitettu opettajalle eikä yleisille lukijoille, jotka etsivät järkevää tai järjestelmällistä luettevaa. 

# Sulatettu Internet 

Kansiot Project 3 ja Project 4 pitävät sisällään omat README.md tiedostot ja itse työt. 

## QT Creator App
Tää Qt appi on tehty timestampeilla, mutta teko onnistuisi myös erisillä date ja time muuttujilla kuten project 3 kanssa. Päädyin kuitenkin tekemään erillisen databasen locaalisesti postgresql:llä, koska sulatettu internet ja rt-linux ohjeissa oli molemmista maininta --> teen 1 generisen qt apin jota voi muokata tarpeen mukaa. Ainut oli vaan et en saanut sitä toimimaan influx databasen kaa. 1 sql driver kyllä lataa mut comandit ei mee perille asti tai ei vaan toimi... DB random value Insert pythonscript ja table rakenne löytyy qt-task/db kansiosta.

Tässä on kuitenkin step by step kuvat databasen teosta:

![dbcreate](/qt-task/db/dbcreate.png)

![tablecreate](/qt-task/db/tablecreate.png)

![datainsert](/qt-task/db/tablecontent.png)

QT Appin kuvat:

![appdallas](/qt-task/db/appdallas.png)

![appdht](/qt-task/db/appdht.png)

![appmpl](/qt-task/db/appmpl.png)

Graafit:

![dallastemp](/qt-task/db/dallastemp.png)

![dhttemp](/qt-task/db/dhttemp.png)

![dhthumi](/qt-task/db/dhthumi.png)

![mpltemp](/qt-task/db/mpltemp.png)

![mplhpa](/qt-task/db/mplhpa.png)

![mplaltitude](/qt-task/db/mplalti.png)
