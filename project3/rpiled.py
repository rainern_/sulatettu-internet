import RPi.GPIO as GPIO
import sys

# Get temperature form subprocess arg
Temperature = float(sys.argv[1])

# to use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BCM)

# Disable warnings setup warnings that can't be avoided
GPIO.setwarnings(False)

# Gpio Pins:
# 17 = Low Led
# 27 = Normal Led
# 22 = High Led
GPIO.setup(17, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)

# Light a LED depending on temperature
if Temperature < 27.0:
    # print("Low")
    GPIO.output(17, 0)
    GPIO.output(27, 1)
    GPIO.output(22, 1)
elif Temperature > 29.0:
    # print("high")
    GPIO.output(17, 1)
    GPIO.output(27, 0)
    GPIO.output(22, 1)
else:
    # print("Normal")
    GPIO.output(17, 1)
    GPIO.output(27, 1)
    GPIO.output(22, 0)

