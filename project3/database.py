from datetime import datetime
import psycopg2

def insertData(temperature):
    # Date, time and database variables
    now = datetime.now()
    time = now.strftime("%H:%M:%S")
    date = now.strftime("%m/%d/%Y")
    conn = None
    sql = "INSERT INTO dallas_data(temperature, date, time) VALUES (%s,%s,%s);"

    try:
        # Connect to the database
        conn = psycopg2.connect(host="localhost",database="project3",user="pi",password="pass")
        # Create a new cursor
        cur = conn.cursor()
        # Execute the INSERT statement
        cur.execute(sql, (temperature, date, time))
        # Commit the changes
        conn.commit()
        # Close communication
        cur.close()

    # Print error if operation failed
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    # Print success message and close connection if it isn't closed
    finally:
        if conn is not None:
            conn.close()
        print("Database insert success")
