import os, sys, subprocess
from time import sleep
import dallasTemp, database, dht_22

lastTemp = 0.0
firstRun = True

# Gpio Pins:
# 17 = Low Led
# 27 = Normal Led
# 22 = High Led
# 18 = DHT22 input
# 4 = Dallas 18B20 input

while True:

    r, w = os.pipe() 
    # PID fuction
    processid = os.fork()

    # Parent Process
    if processid:  
        # Close write pipe
        os.close(w)
        # Setup read fuction
        r = os.fdopen(r)
        # Wait for the child process
        os.waitpid(processid,0)
        # Read pipe and close read pipe
        Temperature = r.read()
        r.close()

        # Subprocess to open led switch script on seperate process
        databaseIns = subprocess.Popen("python3 /home/pi/koulu/sulatettu-internet/project3/rpiled.py " + Temperature,  shell=True)
        Temperature = float(Temperature)

        # Check if there is a compiled file. if not then compile it
        if os.path.isfile("/home/pi/koulu/sulatettu-internet/project3/dht22") == False:
            comp = subprocess.Popen("gcc -o  /home/pi/koulu/sulatettu-internet/project3/dht22 /home/pi/koulu/sulatettu-internet/project3/dht.c -lwiringPi -lwiringPiDev", shell=True)
            comp.wait()
            
        # Get DHT22 Value from c-code and process it
        dhtData = subprocess.check_output("/home/pi/koulu/sulatettu-internet/project3/dht22")
        firstRun, lastTemp = dht_22.process(dhtData, firstRun, lastTemp)

        # Insert Temperature data to database
        database.insertData(Temperature)
        # Wait for the database subprocess to be done 
        databaseIns.wait()
        # Sleep for 5min 
        sleep(300)

    # Child Process
    else:
        # Close read pipe
        os.close(r)
        # Setup write fuction
        w = os.fdopen(w, 'w')
        # Get Temperature
        tempSensor = dallasTemp.read_temp()
        # Write temperature to pipe and close write pipe
        w.write(str(tempSensor))
        w.close()
        # Exit Success
        sys.exit(0)
