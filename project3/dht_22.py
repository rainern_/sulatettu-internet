def process(dhtData, firstRun, lastTemp):
    # Format data 
    dhtString = str(dhtData)
    dhtString = dhtString.replace("b","")
    dhtString = dhtString.replace("'","",2)
    
    # Varify if data is valid
    if dhtString != "Invalid DHT22 Sensor Data":
        dhtArray = dhtString.split(",")
        # Change array strings to float variables
        dhtTemperature = float(dhtArray[0])
        dhtHumidity = float(dhtArray[1])

        # Temperature warning system 
        if firstRun == True:
            firstRun = False
        else:
            if dhtTemperature - lastTemp >= 5:
                print("WARNING Temperature has risen by ", dhtTemperature - lastTemp)
            elif lastTemp - dhtTemperature >= 5:
                print(" WARNING Temperature has lowered by ", lastTemp - dhtTemperature)
        lastTemp = dhtTemperature
        # return values that need to be saved
        return firstRun, lastTemp
    # Print error message if DHT22 data is not valid
    else:
        print(dhtString)
        # return values that need to be saved
        return firstRun, lastTemp