# Project 3 Step By Step

## 1. Setup
1. Tarkistetaan updatet

![Updates](/project3/img/update.png)

2. Tarkista, etta gpiozero on asenettu
![pinout](/project3/img/pinout.png)

3. Itse asensin visual studio:n text editoriksi:
    sudo apt install code


## 2. LEDs
1. Luodaan erilinen python file, joka sisaltaa erilliset led toiminta fiunctiot:
![LED-functions](/project3/img/leds.png)
gpioZero led functioiden info loytyy taalta: https://gpiozero.readthedocs.io/en/stable/index.html

## 3. Dallas 18B20 
1. Laita 1-Wire toiminto paalle ja reboot, jos on tehnty muutoksia: 
![1-Wire Status](/project3/img/1-wire.png)

2. Terminaali tarkistus:
![1-Wire](/project3/img/wire-con.png)

3. Koodi sovellus: 
![1-Wire](/project3/img/dallas-code.png)

## 4. Main Program
2. Luodaaan main ohjelma, joka hoitaa functioiden kutsumisen ja prosessien toiminnan(pipe ja fork) Huom. Work in progress
![Main code 1](/project3/img/main1.png)

Ongelma: GpioZero tai rpiLED aliohjelmat eivat toimi forkin sisalla

Ratkaisu: Kutsutaan prosessi parentissa subprocess.Popen kutsulla eli kuin terminaalissa ja annetaan komennon mukana temperature joka saatiin child prosessista pipella 

Main:

![Main code 2](/project3/img/main2.png)

Led toiminta koodi:

![Led code](/project3/img/rpiledCode.png)


## 5. Database
1. Asennetaan PostgreSQL ja psycopg2 (database adapter) + sen depencyt
    sudo apt install postgresql
    sudo apt install libpq-dev
    sudo apt install python-dev
    pip3 install psycopg2

2. Testataan psql komenolla:
![psql](/project3/img/psql.png)

3. Vaihdetaan pi kayttajaan

    sudo su postgre
    createuser pi -P --interactive
    Enter password for new role:
    Enter it again:
    Shall the new role be a superuser? (y/n) y
    createdb pi;

    exit

4. Retest 
![psqlfix](/project3/img/psql-fix.png)

5. Luodaan talle osiolle oma database ja table
Table:

    CREATE TABLE dallas_data( 
        id  serial PRIMARY KEY,
        temperature NUMERIC NOT NULL,
        date DATE NOT NULL,
        time TIME NOT NULL
    );

![Database](/project3/img/database.png)

6. Luodaan insert koodi osa:
![database code](/project3/img/datacode.png)

7. Testataan toimivuutta
![database code](/project3/img/savedata.png)

## 6. DHT22 Implemitointi
1. Tarkistetaan ja paivitetaan wiringPi tarvittaessa:
![wiringPi update](/project3/img/wiringpi.png)

2. Tehdaan dht luku ohjelma esimerkin perusteella (dht.c) ja testataan:
![dht test](/project3/img/dht2.png)

3. Implemetointi python koodiin (Lampo varotus ja varmistetaa, etta file on olemassa)
![dht process](/project3/img/dhtProcess.png)
![dht file check](/project3/img/dhtCheck.png)

4. Testaus ilman database ominaisuutta:
![dht python test](/project3/img/dht1.png)

Huom! Hyvin usein dht ei saa kunnollisia arvoja 3.3V taikka 5V:lla
