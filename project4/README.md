# Project 4 Step By Step

## Huomiot
- Monet dokumentin linkit ei toiminu: tein projektin periaateen pohjalla: esp32 lähettää dataa mqtt:llä ja raspi ottaa datan, laitaa sen databaseen(influxdb) ja grafana voi visualisoida sitä. Jokainen ominaisuus raspissa on dokcer pohjalla.
- Suurin osa tästä on kirjotettu raspilla ja käytän ansi näppäimistöä --> ä = a ja ö = o yleensä...

## Setup
1. Install Docker ja Runner (Jaettu ohjeet)

![Runner](/project4/img/pub/runner.png)
   
Runner on mielestäni optional.On tarpeellinen, jos haluaa vetää docker-compose runner valinnan. Itse en käyttänyt sitä, kun asetin containereille restart ominaisuuden

## MQTT Broker

1. Kaytetaan brokerian Eclipse-Mosquittoa:
https://hub.docker.com/_/eclipse-mosquitto 

2. Luodaan config ja use filet:
 ![folders](/project4/img/mosqfolders.png)
 Conf-File
 ![conf-file](/project4/img/mosqconf.png)

3. Asennus Command:
   
        sudo docker run --name=mqtt_broker_project4 -itd --restart=always --net=host -v /home/pi/docker/mosquitto-project4/config -v /home/pi/docker/mosquitto-project4/data -v /home/pi/docker/mosquitto-project4/log -v /home/pi/docker/mosquitto-project4/config/mosquitto.conf:/mosquitto/config/mosquitto.conf eclipse-mosquitto

Docker "image" nimetaan mqtt_broker_project4, se on eclipse-mosquitto image. network on hostin ja -v osuudet on volume maaritukset kansioille ja conf filelle. Ja aina kaynnistyy automaattisesti systeemin alkaessa 

Check:
![broker](/project4/img/brokerCheck.png)


## ESP32 (Publisher)

1. Asennetaan Espin kirjastot:

![esp32Setup1](/project4/img/pub/espIn.png)

![esp32Setup2](/project4/img/pub/espIn2.png)

2. Testataan Sensorien toiminta:

![sensorTest](/project4/img/pub/sensorTest.png)

Koodi Osat:
![Main](/project4/img/pub/testMain.png)
![DHT](/project4/img/pub/testDHT.png)
![Dallas](/project4/img/pub/testDallas.png)
![Altimeter](/project4/img/pub/testAltimeter.png)

3. Lisätään mqtt koodi + testaus, jos broker menee down

![MQTT Test](/project4/img/pub/mqtt-connection_test2.png)
![MQTT broker off](/project4/img/pub/publishTest.png)

4. Siivota koodi ja poistaa extra printit eli tämänhetkinen koodi

## Python Data Relay (Subscriber)
Huom Normaalisti ei asennetaisi mitään suoraan koneele jos käytettäisiin dockeria, mutta halusin erottaa nämä 2 prosessia ongelmien korjaus ja oppimis prosessin parantamiseksi

1. Luodaan normaali python script, joka toimii subscriberina

2. Otetaan mqtt data ja insertitetaan se databasenn json muodossa riipuen mqtt topicista. Valmis versio on kansiossa publish.

3. Luodaan scriptille docker implemitaatio

Luodaan docker file subscribe kansioon:
![dockerfile](/project4/img/dockerfile.png)


Luodaan Docker Image docker filen perusteella_

        sudo docker build -t subscribe

Parannuksia olisi, että pip asennukset tehtäisiin pythonin virtual enviromentissa.

![docker build1](/project4/img/dockerBuildp1.png)
![docker build2](/project4/img/dockerBuildp2.png)

Docker run container:

        sudo docker run -it subscribe subscribe.py -d --name="subscribe_project4"

Restart ja background functiot ei toimi tässä, joten tekisin cmd/terminal scriptin jos haluisin runnitaa tän containerin raspin käynnistyksessä tai docker compose koko setti voisi ratkaista tämän "ongelman"

![Docker Python Test](/project4/img/dockerPYtest.png)

1. Tarkistetaan dockerin data insert 

![database Test](/project4/img/databaseTest.png)

## Influxdb (Database)
1. Pullataan arm 32 bit versio influxdb:sta:

![pull influx](/project4/img/pullInflux.png)

2. Luodaan kaytto dirrectory

        mkdir /home/pi/docker/influxdb/

3. Luodaan docker container komennolla:
    
        docker run --name=influxdb_projet4 restart=always -d -p 8086:8086 -v /home/pi/docker/influxdb/:/var/lib/influxdb2 arm32v7/influxdb:latest

eli pyorii taustalla, alttaa itsensa kaynistyksessa, volume on paikassa joka tehtiin ja portti ja nimi asetukset

![influx container](/project4/img/influxContainer.png)

4. Päästaan shelliin container id:lla:

        docker exec -it 6b3e57c751f2 influx

![influx shell](/project4/img/influxShell.png)

## Grafana
1. Pullataann docker image:

        docker pull grafana/grafana

![Grafana Image Pull](/project4/img/grafanaPull.png)

2. Runataan/luodaan docker container:

        docker run -d --name=grafana -p 3000:3000 grafana/grafana

Itse lisäisin --restart=always että container aloittaa aina kun sysyteemi käynnistetään...

![Grafana run](/project4/img/grafanaRun.png)

1. "Asetetaan" grafanaan uusi database mistä lukea
   
![GrafanaDBcon1](/project4/img/grafanaCon1.png)
![GrafanaDBcon2](/project4/img/grafanaCon2.png)
![GrafanaDBcon3](/project4/img/grafanaCon3.png)

4. Määrittää uusi dashboard tyhjästä käyttäen ennen mainituilla tageilla:

![Grafana Dasboard](/project4/img/grafanaDasboard.png)
