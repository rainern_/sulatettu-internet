import time
import paho.mqtt.client as paho
from influxdb import InfluxDBClient

def insertData(device, sensor, measure, value):
    json_body = [
        {
            "measurement": "MeasureEvent",
            "tags": {
                "device": device,
                "sensor": sensor,
                "measuring": measure
            },
            "fields":{
                "value": value
            }     
        }
    ]

    if dbclient.write_points(json_body) == False:
        print("Database data insert failed")
    else:
        print("Done")


# Broker IP/Address
broker="192.168.1.107"

#define callback
def on_message(client, userdata, message):
    messageValue = float(message.payload.decode("utf-8"))
    device = "esp32"
    # Do action based on subscribe topic
    if message.topic == "esp32/dht22/humidity":
        sensor = "DHT22"
        measure = "Humidity"
    elif message.topic == "esp32/dht22/temperature":
        sensor = "DHT22"
        measure = "Temperature"
    elif message.topic == "esp32/dallas/temperature":
        sensor = "18B20"
        measure = "Temperature"
    elif message.topic == "esp32/altimeter/pascal":
        sensor = "MPL3115A2"
        measure = "hPa"
    elif message.topic == "esp32/altimeter/altitude":
        sensor = "MPL3115A2"
        measure = "Altitude"
    elif message.topic == "esp32/altimeter/temperature":
        sensor = "MPL3115A2"
        measure = "Temperature"

    # Send Value to database
    insertData(device, sensor, measure, messageValue)

# Setup InfluxDB
dbclient = InfluxDBClient(host="192.168.1.107", port=8086)
dbclient.switch_database("project4")

# Create client object
client= paho.Client("RaspberryPi")  
#Bind function to callback
client.on_message=on_message
# Connect to broker, default port = 1883 --> no need of further define
print("connecting to broker ",broker)
client.connect(broker)
# Start loop to process received messages
client.loop_start() 
print("subscribing ")
# Subscribe to all the topics with QoS of 2
client.subscribe("esp32/dht22/humidity",2)
client.subscribe("esp32/dht22/temperature",2)
client.subscribe("esp32/dallas/temperature",2)
client.subscribe("esp32/altimeter/pascal",2)
client.subscribe("esp32/altimeter/altitude",2)
client.subscribe("esp32/altimeter/temperature",2)

# Recieve subscribe messages until KeyboardInterupt --> Ctrl + C
try:
    while True:
        time.sleep(1)
  
except KeyboardInterrupt:
    print ("exiting")
    client.disconnect()
    client.loop_stop()
