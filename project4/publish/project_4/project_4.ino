#include <WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include "DHT.h"
#include <OneWire.h> 
#include <DallasTemperature.h>
#include <Adafruit_MPL3115A2.h>
 
// Wifi Settings 
const char* ssid = "exampleIP";
const char* password = "passwordexample";
 
// MQTT Broker ip/address
const char* mqtt_server = "192.168.1.107";

// Client Setups
WiFiClient espClient;
PubSubClient client(espClient);

//Global Variable Setup
long lastMsg = 0;
char msg[50];
int value = 0;

// Define DHT22
#define DHTPIN 2
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

// Define Dallas 18b20
#define ONE_WIRE_BUS 15
// Create a new instance of the oneWire class to communicate with any OneWire device:
OneWire oneWire(ONE_WIRE_BUS);
// Pass the oneWire reference to DallasTemperature library:
DallasTemperature dallas(&oneWire);

// Define MPL3115A2
Adafruit_MPL3115A2 baro = Adafruit_MPL3115A2();


// Setup "calls"
void setup() {
  Serial.begin(9600); 
  dht.begin();
  dallas.begin();
  setup_wifi();
  client.setServer(mqtt_server, 1883);
}

// Wifi setup function
void setup_wifi() {
  delay(10);
  // Connect to Wifi
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// Reconnect function  
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {
  // reconnect if no connection  
  if (!client.connected()) {
    reconnect();
  }
  //mqtt loop  
  client.loop();
 
  long now = millis();
  // 5s publish set delay  
  if (now - lastMsg > 5000) {
    lastMsg = now;

    // Read Sensors & Publish     
    readDHT();
    readDallas();
    readAltimeter();
  }
}

// Read DHT22 Sensor function + mqtt publish
void readDHT(){
  float humidityDHT = dht.readHumidity();
  float temperatureDHT = dht.readTemperature();
 
  // check if returns are valid and print the sensor data
  if (isnan(temperatureDHT) || isnan(humidityDHT)) {
    Serial.println("Failed to read from DHT");
  } else {
    // Convert the value to a char 
    char dhthString[8], dhttString[8];
    dtostrf(humidityDHT, 1, 2, dhthString);
    dtostrf(temperatureDHT, 1, 2, dhttString);
    // mqtt Publish
    client.publish("esp32/dht22/humidity", dhthString);
    client.publish("esp32/dht22/temperature", dhttString);
  }
}

// Read 18B20 Sensor function + mqtt publish
void readDallas(){
  // Send the command for all devices on the bus to perform a temperature conversion:
  dallas.requestTemperatures();
  // Fetch the temperature in degrees Celsius for device index:
  float tempC = dallas.getTempCByIndex(0); // the index 0 refers to the first device
  // Fetch the temperature in degrees Fahrenheit for device index:
  float tempF = dallas.getTempFByIndex(0);

  // Confrim that sensor read
  if (isnan(tempC)) {
    Serial.println("Failed to read from Dallas");
  } else {
    // Convert the value to a char
    char daltString[8];
    dtostrf(tempC, 1, 2, daltString);
    // mqtt Publish
    client.publish("esp32/dallas/temperature", daltString);
  }
}

// Read MPL3115A2 Sensor function + mqtt publish
void readAltimeter(){
  if (! baro.begin()) {
    Serial.println("Couldnt find MPL3115A2");
  }else{
    float pascals = baro.getPressure();
    float altm = baro.getAltitude(); 
    float tempC = baro.getTemperature();

    //Muutos hPa:ksi
    pascals = pascals/100;
    
    // Convert the value to a char
    char pasString[8], altString[8], atempString[8];
    dtostrf(pascals, 1, 2, pasString);
    dtostrf(altm, 1, 2, altString);
    dtostrf(tempC, 1, 2, atempString);
    // mqtt Publish
    client.publish("esp32/altimeter/pascal", pasString);
    client.publish("esp32/altimeter/altitude", altString);
    client.publish("esp32/altimeter/temperature", atempString);
    }
}
