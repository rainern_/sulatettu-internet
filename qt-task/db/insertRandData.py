from datetime import datetime, timezone
import psycopg2
import random
import time

conn = None
dalsql = "INSERT INTO dallas_data(temperature, date, time, timestamp) VALUES (%s,%s,%s,%s);"
dhtsql = "INSERT INTO dht_data(temperature, humidity, date, time, timestamp) VALUES (%s,%s,%s,%s,%s);"
altsql = "INSERT INTO alt_data(temperature, hpa, altitude, date, time, timestamp) VALUES (%s,%s,%s,%s,%s,%s);"
try:
    # Connect to the database
    conn = psycopg2.connect(host="localhost",database="dataproject",user="rane",password="chM6E89LDRX&B#HN")
    # Create a new cursor
    cur = conn.cursor()
    # Dallas
    for x in range(10):
        # Date, time and database variables
        timestamp = datetime.now()
        times = timestamp.strftime("%H:%M:%S")
        date = timestamp.strftime("%d/%m/%Y")

        temperature = round(random.uniform(-20, 40), 2)
        # Execute the INSERT statement
        cur.execute(dalsql, (temperature, date, times, timestamp))
        # Commit the changes
        conn.commit()

        # DHT
        temperature = round(random.uniform(-20, 40), 2)
        humidity = round(random.uniform(0, 100))
        # Execute the INSERT statement
        cur.execute(dhtsql, (temperature, humidity, date, times, timestamp))
        # Commit the changes
        conn.commit()

        # Altitude
        temperature = round(random.uniform(-20, 40), 2)
        hpa = round(random.uniform(500, 1500), 2)
        altitude = round(random.uniform(40, 120), 2)
        # Execute the INSERT statement
        cur.execute(altsql, (temperature, hpa, altitude, date, times, timestamp))
        # Commit the changes
        conn.commit()

        # Sleep
        time.sleep(300)

    # Close communication
    cur.close()

# Print error if operation failed
except (Exception, psycopg2.DatabaseError) as error:
    print(error)

# Print success message and close connection if it isn't closed
finally:
    if conn is not None:
        conn.close()
    print("Database insert success")
