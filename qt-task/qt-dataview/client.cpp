#include "client.h"
#include "ui_client.h"

#define selectDallas "SELECT * FROM dallas_data"
#define selectDHT "SELECT * FROM dht_data"
#define selectMPL "SELECT * FROM alt_data"

Client::Client(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Client)
{
    ui->setupUi(this);

    //First launch & Table Set
     ui->terminalBrowser->setText("Connecting to database..."); //terminal process info print
    if(QSqlDatabase::database().open()) //If database is open --> update/pull database info to UI
    {
       qDebug()<<"Opened"; //Debug notification that database open was success
       ui->terminalBrowser->append("Success");  //terminal process info print
       QSqlQueryModel*modal=new QSqlQueryModel();
       QSqlQuery*qry=new QSqlQuery(QSqlDatabase::database());
       qry->prepare(selectDallas);
       qry->exec();
       modal->setQuery(*qry);
       ui->databaseTable->setModel(modal); //Info to console "screen"
       QSqlDatabase::database().close(); //Close Database Access
       qDebug()<<"Close Success\nYou can exit safely";
       ui->terminalBrowser->append("Connection closed \nYou are safe to close aplication\n"); //print to terminal you can exit
    }

    else //If database didn't open, then displkay error message & what went wrong
    {
        qDebug()<<"Error = "<<QSqlDatabase::database().lastError().text();
        ui->terminalBrowser->append("Connection to database failed");
    }
}

Client::~Client()
{
    delete ui;
}

//Graph button action
void Client::on_graphButton_clicked()
{
    //convert sensorbox value to string
    QString sbText = ui->sensorBox ->currentText();

    //Check String & do action based on that
    if(sbText=="Dallas Temperature")
    {
        dallasTemperature();
    }
    else if(sbText=="DHT Temperature")
    {
        dhtTemperature();
    }
    else if(sbText=="DHT Humidity")
    {
        dhtHumidity();
    }
    else if(sbText=="MPL Temperature")
    {
        mplTemperature();
    }
    else if(sbText=="MPL hPa")
    {
        mplHpa();
    }
    else if(sbText=="MPL Altitude")
    {
        mplAltitude();
    }
}

//refresh button action
void Client::on_refreshButton_clicked()
{
    ui->terminalBrowser->append("Refreshing database...");  //terminal process info print
    if (QSqlDatabase::database().open()) //If database is open --> update/pull database info to UI
    {
       qDebug()<<"Opened"; //Debug notificatiion that database open was success
       QSqlQueryModel*modal=new QSqlQueryModel();
       QSqlQuery*qry=new QSqlQuery(QSqlDatabase::database());

       //convert sensorbox value to string
       QString sbText = ui->sensorBox ->currentText();

       //Check String & do action based on that
       if(sbText=="Dallas Temperature")
       {
           qry->prepare(selectDallas);
       }
       else if(sbText=="DHT Temperature" || sbText=="DHT Humidity")
       {
           qry->prepare(selectDHT);
       }
       else if(sbText=="MPL Temperature" || sbText=="MPL hPa" ||sbText=="MPL Altitude")
       {
           qry->prepare(selectMPL);
       }
       qry->exec();
       modal->setQuery(*qry);
       ui->databaseTable->setModel(modal); //Info to console "screen"
       QSqlDatabase::database().close(); //Close Database Access
       qDebug()<<"Close Success\nYou can exit safely";
       ui->terminalBrowser->append("Refreshing database completed \nYou are safe to close aplication\n");  //terminal process info print
    }

    else //If database didn't open, then displkay error message & what went wrong
    {
        qDebug()<<"Error = "<<QSqlDatabase::database().lastError().text();
        ui->terminalBrowser->append("Refreshing database failed");  //terminal process info print
    }
}

//Dallas Temperature Chart
void Client::dallasTemperature()
{
    //terminal update what has happened
    ui->terminalBrowser->append("Opening Dallas Temperature Chart...");

    //Chart Value Setup
    QLineSeries *series = new QLineSeries();
    QSqlQueryModel*modal=new QSqlQueryModel();
    QDateTime xValue;

    //Open Database to use data for chart
    if(QSqlDatabase::database().open()) //If database is open --> update/pull database info to UI
    {
       qDebug()<<"Opened"; //Debug notificatiion that database open was success

       QSqlQuery*qry=new QSqlQuery(QSqlDatabase::database());
       qry->prepare(selectDallas);
       qry->exec();

       //Go through database and add chart "points"
       while (qry->next()) {
           QSqlRecord record = qry->record();
           xValue.setDate(QDate(record.value("timestamp").toDate().year(), record.value("timestamp").toDate().month() , record.value("timestamp").toDate().day()));
           xValue.setTime(QTime(record.value("timestamp").toTime().hour(), record.value("timestamp").toTime().minute()));
           series->append(xValue.toMSecsSinceEpoch(), record.value("temperature").toFloat());
       }

       //Update console
       modal->setQuery(*qry);
       ui->databaseTable->setModel(modal); //Info to console "screen"

       //Close database & Inform user
       QSqlDatabase::database().close(); //Close Database Access
       qDebug()<<"Close Success\nYou can exit safely";
       ui->terminalBrowser->append("Success \nYou are safe to close aplication\n");  //terminal process info print
    }

    //If database didn't open, then displkay error message & what went wrong
    else
    {
        qDebug()<<"Error = "<<QSqlDatabase::database().lastError().text();
        ui->terminalBrowser->append("Failure. Connection to database failed");  //terminal process info print
    }

    //create chart
    QChartView *chartView = new QChartView;
    chartView->chart()->addSeries(series);
    chartView->chart()->setAnimationOptions(QChart::AllAnimations);
    chartView->chart()->legend()->hide();
    chartView->chart()->setTitle("Dallas 18B20");

    //X-axis Setup
    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setFormat("dd-MM-yyyy h:mm");
    axisX->setTitleText("Date & Time");
    //chartView->chart()->setAxisX(axisX, series);
    chartView->chart()->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    //Y-axis Setup
    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%.2f C");
    axisY->setTitleText("Temperature");
    chartView->chart()->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    //name,resize & show/start chart window
    chartView->setWindowTitle("Dallas Temperature Chart");
    chartView->resize(700,400);
    chartView->show();
}


//DHT Temperature Chart
void Client::dhtTemperature()
{
     //terminal update what has happened
     ui->terminalBrowser->append("Opening DHT Temperature Chart...");

     //Chart Value Setup
     QLineSeries *series = new QLineSeries();
     QSqlQueryModel*modal=new QSqlQueryModel();
     QDateTime xValue;

     //Open Database to use data for chart
     if(QSqlDatabase::database().open()) //If database is open --> update/pull database info to UI
     {
        qDebug()<<"Opened"; //Debug notificatiion that database open was success

        QSqlQuery*qry=new QSqlQuery(QSqlDatabase::database());
        qry->prepare(selectDHT);
        qry->exec();

        //Go through database and add chart "points"
        while (qry->next()) {
            QSqlRecord record = qry->record();
            xValue.setDate(QDate(record.value("timestamp").toDate().year(), record.value("timestamp").toDate().month() , record.value("timestamp").toDate().day()));
            xValue.setTime(QTime(record.value("timestamp").toTime().hour(), record.value("timestamp").toTime().minute()));
            series->append(xValue.toMSecsSinceEpoch(),record.value("temperature").toFloat());
        }

        //Update console
        modal->setQuery(*qry);
        ui->databaseTable->setModel(modal); //Info to console "screen"

        //Close database & Inform user
        QSqlDatabase::database().close(); //Close Database Access
        qDebug()<<"Close Success\nYou can exit safely";
        ui->terminalBrowser->append("Success \nYou are safe to close aplication\n");  //terminal process info print
     }

     //If database didn't open, then displkay error message & what went wrong
     else
     {
         qDebug()<<"Error = "<<QSqlDatabase::database().lastError().text();
         ui->terminalBrowser->append("Failure. Connection to database failed");  //terminal process info print
     }

     //create chart
     QChartView *chartView = new QChartView;
     chartView->chart()->addSeries(series);
     chartView->chart()->setAnimationOptions(QChart::AllAnimations);
     chartView->chart()->legend()->hide();
     chartView->chart()->setTitle("DHT 22");

     //X-axis Setup
     QDateTimeAxis *axisX = new QDateTimeAxis;
     axisX->setFormat("dd-MM-yyyy h:mm");
     axisX->setTitleText("Date & Time");
     chartView->chart()->addAxis(axisX, Qt::AlignBottom);
     series->attachAxis(axisX);

     //Y-axis Setup
     QValueAxis *axisY = new QValueAxis;
     axisY->setLabelFormat("%.2f C");
     axisY->setTitleText("Temperature");
     chartView->chart()->addAxis(axisY, Qt::AlignLeft);
     series->attachAxis(axisY);

     //name,resize & show/start chart window
     chartView->setWindowTitle("DHT Temperature Chart");
     chartView->resize(700,400);
     chartView->show();
}


//DHT Humidity Chart
void Client::dhtHumidity()
{
     ////terminal update what has happened
     ui->terminalBrowser->append("Opening DHT Humidity Chart...");

     //Chart Value Setup
     QLineSeries *series = new QLineSeries();
     QSqlQueryModel*modal=new QSqlQueryModel();
     QDateTime xValue;

     //Open Database to use data for chart
     if(QSqlDatabase::database().open()) //If database is open --> update/pull database info to UI
     {
        qDebug()<<"Opened"; //Debug notificatiion that database open was success

        QSqlQuery*qry=new QSqlQuery(QSqlDatabase::database());
        qry->prepare(selectDHT);
        qry->exec();

        //Go through database and add chart "points"
        while (qry->next()) {
            QSqlRecord record = qry->record();
            xValue.setDate(QDate(record.value("timestamp").toDate().year(), record.value("timestamp").toDate().month() , record.value("timestamp").toDate().day()));
            xValue.setTime(QTime(record.value("timestamp").toTime().hour(), record.value("timestamp").toTime().minute()));
            series->append(xValue.toMSecsSinceEpoch(), record.value("humidity").toFloat());
        }

        //Update console
        modal->setQuery(*qry);
        ui->databaseTable->setModel(modal); //Info to console "screen"

        //Close database & Inform user
        QSqlDatabase::database().close(); //Close Database Access
        qDebug()<<"Close Success\nYou can exit safely";
        ui->terminalBrowser->append("Success \nYou are safe to close aplication\n");  //terminal process info print
     }

     //If database didn't open, then displkay error message & what went wrong
     else
     {
         qDebug()<<"Error = "<<QSqlDatabase::database().lastError().text();
         ui->terminalBrowser->append("Failure. Connection to database failed");  //terminal process info print
     }

     //create chart
     QChartView *chartView = new QChartView;
     chartView->chart()->addSeries(series);
     chartView->chart()->setAnimationOptions(QChart::AllAnimations);
     chartView->chart()->legend()->hide();
     chartView->chart()->setTitle("DHT 22");

     //X-axis Setup
     QDateTimeAxis *axisX = new QDateTimeAxis;
     axisX->setFormat("dd-MM-yyyy h:mm");
     axisX->setTitleText("Date & Time");
     chartView->chart()->addAxis(axisX, Qt::AlignBottom);
     series->attachAxis(axisX);

     //Y-axis Setup
     QValueAxis *axisY = new QValueAxis;
     axisY->setLabelFormat("%.2f %");
     axisY->setTitleText("Humidity");
     chartView->chart()->addAxis(axisY, Qt::AlignLeft);
     series->attachAxis(axisY);

     //name,resize & show/start chart window
     chartView->setWindowTitle("DHT Humidity Chart");
     chartView->resize(700,400);
     chartView->show();
}

//MPL Temperature Chart
void Client::mplTemperature()
{
     //terminal update what has happened
     ui->terminalBrowser->append("Opening MPL Temperature Chart...");

     //Chart Value Setup
     QLineSeries *series = new QLineSeries();
     QSqlQueryModel*modal=new QSqlQueryModel();
     QDateTime xValue;

     //Open Database to use data for chart
     if(QSqlDatabase::database().open()) //If database is open --> update/pull database info to UI
     {
        qDebug()<<"Opened"; //Debug notificatiion that database open was success

        QSqlQuery*qry=new QSqlQuery(QSqlDatabase::database());
        qry->prepare(selectMPL);
        qry->exec();

        //Go through database and add chart "points"
        while (qry->next()) {
            QSqlRecord record = qry->record();
            xValue.setDate(QDate(record.value("timestamp").toDate().year(), record.value("timestamp").toDate().month() , record.value("timestamp").toDate().day()));
            xValue.setTime(QTime(record.value("timestamp").toTime().hour(), record.value("timestamp").toTime().minute()));
            series->append(xValue.toMSecsSinceEpoch(),record.value("temperature").toFloat());
        }

        //Update console
        modal->setQuery(*qry);
        ui->databaseTable->setModel(modal); //Info to console "screen"

        //Close database & Inform user
        QSqlDatabase::database().close(); //Close Database Access
        qDebug()<<"Close Success\nYou can exit safely";
        ui->terminalBrowser->append("Success \nYou are safe to close aplication\n");  //terminal process info print
     }

     //If database didn't open, then displkay error message & what went wrong
     else
     {
         qDebug()<<"Error = "<<QSqlDatabase::database().lastError().text();
         ui->terminalBrowser->append("Failure. Connection to database failed");  //terminal process info print
     }

     //create chart
     QChartView *chartView = new QChartView;
     chartView->chart()->addSeries(series);
     chartView->chart()->setAnimationOptions(QChart::AllAnimations);
     chartView->chart()->legend()->hide();
     chartView->chart()->setTitle("MPL3115A2");

     //X-axis Setup
     QDateTimeAxis *axisX = new QDateTimeAxis;
     axisX->setFormat("dd-MM-yyyy h:mm");
     axisX->setTitleText("Date & Time");
     chartView->chart()->addAxis(axisX, Qt::AlignBottom);
     series->attachAxis(axisX);

     //Y-axis Setup
     QValueAxis *axisY = new QValueAxis;
     axisY->setLabelFormat("%.2f C");
     axisY->setTitleText("Temperature");
     chartView->chart()->addAxis(axisY, Qt::AlignLeft);
     series->attachAxis(axisY);

     //name,resize & show/start chart window
     chartView->setWindowTitle("MPL Temperature Chart");
     chartView->resize(700,400);
     chartView->show();
}

//MPL hPa Chart
void Client::mplHpa()
{
     //terminal update what has happened
     ui->terminalBrowser->append("Opening MPL hPa Chart...");

     //Chart Value Setup
     QLineSeries *series = new QLineSeries();
     QSqlQueryModel*modal=new QSqlQueryModel();
     QDateTime xValue;

     //Open Database to use data for chart
     if(QSqlDatabase::database().open()) //If database is open --> update/pull database info to UI
     {
        qDebug()<<"Opened"; //Debug notificatiion that database open was success

        QSqlQuery*qry=new QSqlQuery(QSqlDatabase::database());
        qry->prepare(selectMPL);
        qry->exec();

        //Go through database and add chart "points"
        while (qry->next()) {
            QSqlRecord record = qry->record();
            xValue.setDate(QDate(record.value("timestamp").toDate().year(), record.value("timestamp").toDate().month() , record.value("timestamp").toDate().day()));
            xValue.setTime(QTime(record.value("timestamp").toTime().hour(), record.value("timestamp").toTime().minute()));
            series->append(xValue.toMSecsSinceEpoch(),record.value("hpa").toFloat());
        }

        //Update console
        modal->setQuery(*qry);
        ui->databaseTable->setModel(modal); //Info to console "screen"

        //Close database & Inform user
        QSqlDatabase::database().close(); //Close Database Access
        qDebug()<<"Close Success\nYou can exit safely";
        ui->terminalBrowser->append("Success \nYou are safe to close aplication\n");  //terminal process info print
     }

     //If database didn't open, then displkay error message & what went wrong
     else
     {
         qDebug()<<"Error = "<<QSqlDatabase::database().lastError().text();
         ui->terminalBrowser->append("Failure. Connection to database failed");  //terminal process info print
     }

     //create chart
     QChartView *chartView = new QChartView;
     chartView->chart()->addSeries(series);
     chartView->chart()->setAnimationOptions(QChart::AllAnimations);
     chartView->chart()->legend()->hide();
     chartView->chart()->setTitle("MPL3115A2");

     //X-axis Setup
     QDateTimeAxis *axisX = new QDateTimeAxis;
     axisX->setFormat("dd-MM-yyyy h:mm");
     axisX->setTitleText("Date & Time");
     chartView->chart()->addAxis(axisX, Qt::AlignBottom);
     series->attachAxis(axisX);

     //Y-axis Setup
     QValueAxis *axisY = new QValueAxis;
     axisY->setLabelFormat("%.2f");
     axisY->setTitleText("hPa");
     chartView->chart()->addAxis(axisY, Qt::AlignLeft);
     series->attachAxis(axisY);

     //name,resize & show/start chart window
     chartView->setWindowTitle("MPL hPa Chart");
     chartView->resize(700,400);
     chartView->show();
}

//MPL Altitude Chart
void Client::mplAltitude()
{
     //terminal update what has happened
     ui->terminalBrowser->append("Opening MPL Altitude Chart...");

     //Chart Value Setup
     QLineSeries *series = new QLineSeries();
     QSqlQueryModel*modal=new QSqlQueryModel();
     QDateTime xValue;

     //Open Database to use data for chart
     if(QSqlDatabase::database().open()) //If database is open --> update/pull database info to UI
     {
        qDebug()<<"Opened"; //Debug notificatiion that database open was success

        QSqlQuery*qry=new QSqlQuery(QSqlDatabase::database());
        qry->prepare(selectMPL);
        qry->exec();

        //Go through database and add chart "points"
        while (qry->next()) {
            QSqlRecord record = qry->record();
            xValue.setDate(QDate(record.value("timestamp").toDate().year(), record.value("timestamp").toDate().month() , record.value("timestamp").toDate().day()));
            xValue.setTime(QTime(record.value("timestamp").toTime().hour(), record.value("timestamp").toTime().minute()));
            series->append(xValue.toMSecsSinceEpoch(),record.value("altitude").toFloat());
        }

        //Update console
        modal->setQuery(*qry);
        ui->databaseTable->setModel(modal); //Info to console "screen"

        //Close database & Inform user
        QSqlDatabase::database().close(); //Close Database Access
        qDebug()<<"Close Success\nYou can exit safely";
        ui->terminalBrowser->append("Success \nYou are safe to close aplication\n");  //terminal process info print
     }

     //If database didn't open, then displkay error message & what went wrong
     else
     {
         qDebug()<<"Error = "<<QSqlDatabase::database().lastError().text();
         ui->terminalBrowser->append("Failure. Connection to database failed");  //terminal process info print
     }

     //create chart
     QChartView *chartView = new QChartView;
     chartView->chart()->addSeries(series);
     chartView->chart()->setAnimationOptions(QChart::AllAnimations);
     chartView->chart()->legend()->hide();
     chartView->chart()->setTitle("MPL3115A2");

     //X-axis Setup
     QDateTimeAxis *axisX = new QDateTimeAxis;
     axisX->setFormat("dd-MM-yyyy h:mm");
     axisX->setTitleText("Date & Time");
     chartView->chart()->addAxis(axisX, Qt::AlignBottom);
     series->attachAxis(axisX);

     //Y-axis Setup
     QValueAxis *axisY = new QValueAxis;
     axisY->setLabelFormat("%.2f");
     axisY->setTitleText("Altitude");
     chartView->chart()->addAxis(axisY, Qt::AlignLeft);
     series->attachAxis(axisY);

     //name,resize & show/start chart window
     chartView->setWindowTitle("MPL Altitude Chart");
     chartView->resize(700,400);
     chartView->show();
}
